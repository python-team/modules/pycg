#!/usr/bin/python2.2
import Cg
import string

context = Cg.cgCreateContext()

if not context: sys.exit(1) ## print "Context is", context

# Load in the program
f = file("simple.cg")
programSource = string.join(f.readlines())

program = Cg.cgCreateProgram(context, Cg.CG_SOURCE, programSource, Cg.CG_PROFILE_ARBVP1, "main", None)
if not program: sys.exit(1) ## print "Program is", program

program = Cg.cgCreateProgram(context, Cg.CG_SOURCE, programSource, Cg.CG_PROFILE_ARBVP1, "main", ["-profileopts", "NumInstructionSlots=3000"])
if not program: sys.exit(1) ## print "Program is", program


Cg.cgDestroyContext(context)
